<?php
abstract class Saleable
{
        protected $vendor;
        protected $productCategory;
        protected $productName;
        protected $productPrice;	
        protected $currencyCode = 'EUR';
        
        public function getVendor() {
            return $this->vendor;
        }
              
        public function setVendor($vendor) {
            $this->vendor = $vendor;
        }
        
        public function getProductCategory() {
            return $this->productCategory;
        }
              
        public function setProductCategory($category) {
            $this->productCategory = $category;
        }      
        
        public function getProductName() {
            return $this->productName;
        }
              
        public function setProductName($name) {
            $this->productName = $name;
        }      
        
        public function getProductPrice() {
            return $this->productPrice;
        }
        
        public function setProductPrice($price) {
            $this->productPrice = $price;
        }
    
        public function getCurrencyCode() {
            return $this->currencyCode;
        }
        
        public function setCurrencyCode($currCode) {
            $this->currencyCode = $currCode;
        }
}

class SuperAuto extends Saleable
{
    protected $hexColor = '#fff';
    protected $VINCode; 
    
    public function __construct($vendor, $price, $category, $name, $vinCode) {
	       $this->setVendor($vendor);
	       $this->setProductPrice($price);
	       $this->setProductCategory($category);
           $this->setProductName($name);
	       $this->setVINCode($vinCode);
	}
	
    public function getHexColor() {
        return $this->hexColor;
    }
    
    public function setHexColor($color) {
        $this->hexColor = $color;
    }
    
    public function getVINCode() {
        return $this->VINCode;
    }
    
    public function setVINCode($vinCode) {
        $this->VINCode = $vinCode;
    }
}

$auto1 = new SuperAuto('Lada',6000,'транспортные средства', 'легковой автомобиль Лада Калина', '1GNEK13ZX3R298984');
$auto1->setCurrencyCode('USD');
echo '<pre>';
var_dump($auto1);

$auto2 = new SuperAuto('BMV',60000,'транспортные средства', 'легковой автомобиль BMV', '5XXGN4A70CG022862');
$auto2->setHexColor('#000');
echo '<pre>';
var_dump($auto2);

class TV extends Saleable
{
    protected $screenDiagonal = 45;
    
    public function __construct($vendor, $price, $category, $name) {
	       $this->setVendor($vendor);
	       $this->setProductPrice($price);
	       $this->setProductCategory($category);
           $this->setProductName($name);
	}     
         
    public function getItsScreenDiagonal() {
        return $this->screenDiagonal;
    }
    
    public function setItsScreenDiagonal($screenDiagonal) {
        $this->screenDiagonal = $screenDiagonal;
    }
}

$tv1 = new TV('Samsung', 1200, 'TV', 'LED Телевизор Samsung UE49M6550');
$tv1->setItsScreenDiagonal(100); 
echo '<pre>';
var_dump($tv1);

$tv2 = new TV('Xiaomi', 1500, 'TV', 'Телевизор Xiaomi Mi TV 4A 49 дюйма');
echo '<pre>';
var_dump($tv2);

class BallPen extends Saleable
{
    protected $hexColor;
            
    public function __construct($vendor, $price, $category, $name) {
	       $this->setVendor($vendor);
	       $this->setProductPrice($price);
	       $this->setProductCategory($category);
           $this->setProductName($name);
	}    
			
    public function getHexColor() {
        return $this->hexColor;
    }
    
    public function setHexColor($color) {
        $this->hexColor = $color;
    }
}

$pen1 = new BallPen('Pilot',60, 'письменные принадлежности','шариковая ручка Pilot');
echo '<pre>';
var_dump($pen1);

$pen2 = new BallPen('Bic',10, 'письменные принадлежности','шариковая ручка Bic');
echo '<pre>';
$pen2->setHexColor('#000');
var_dump($pen2);

class Duck extends Saleable
{
    protected $packagingSize;    
    protected $sinceAge;
                  
    public function __construct($vendor, $price, $category, $name, $age) {
           $this->setVendor($vendor);
           $this->setProductPrice($price);
           $this->setProductCategory($category);
           $this->setProductName($name);
           $this->setSinceAge($age);
    }    
            
    public function getSinceAge() {
        return $this->sinceAge;
    }
    
    public function setSinceAge($age) {
        $this->sinceAge = $age;
    }
    
    public function getPackagingSize() {
        return $this->packagingSize;
    }
    
    public function setPackagingSize($size) {
        $this->packagingSize = $size;
    }
}

$duck1 = new Duck('Schleich',15, 'фигурка','Schleich Фигурка Утка', 3);
echo '<pre>';
$duck1->setPackagingSize('5 x 5 x 3');
var_dump($duck1);

$duck2 = new Duck('Funny Ducks',5, 'игрушка для ванной','Funny Ducks Игрушка для ванной Уточка Ангел', 3);
echo '<pre>';
$duck2->setPackagingSize('10 x 9 x 8');
var_dump($duck2);


class Product extends Saleable
{
    protected $amount;
    protected $packagingSize;    
    protected $validityTime;
                  
    public function __construct($vendor, $price, $category, $name, $amount, 
        $size, $time) {
        $this->setVendor($vendor);
        $this->setProductPrice($price);
        $this->setProductCategory($category);
        $this->setProductName($name);
        $this->setAmount($amount);    
        $this->setPackagingSize($size);    
        $this->setValidityTime($time);
    }    
    
    public function getAmount() {
        return $this->amount;
    }
    
    public function setAmount($amount) {
        $this->amount = $amount;
    }
        
    public function getPackagingSize() {
        return $this->amount;
    }
    
    public function setPackagingSize($size) {
        $this->packagingSize = $size;
    }
              
    public function getValidityTime() {
        return $this->amount;
    }
    
    public function setValidityTime($time) {
        $this->validityTime = $time;
    }
}

$product1 = new Product('ОАО "Вимм-Биль-Данн"',2, 'Молоко и сливки','Молоко Домик в деревне топленое 3,2%', 100 ,'95×65×165 мм (Д×Ш×В)','2018-10-15');
echo '<pre>';
var_dump($product1);

$product2 = new Product('Бель Франц',20, 'Варенье, фрукты в сиропе, мёд','Джем Belle France Клубничный экстра', 450, '91×91×100 мм (Д×Ш×В)', '2018-11-15');
echo '<pre>';
var_dump($product2);